namespace :informant do
  desc 'Verify connectivity from your app to Informant'
  task diagnostic: :environment do
    Rails.logger = Logger.new($stdout)
    InformantRails::Diagnostic.run
  end
end
