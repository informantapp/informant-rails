if defined?(Rake)
  require 'rake'
  Dir[File.join(File.dirname(__FILE__), 'tasks', '**/*.rake')].each { |rake| load rake }
end

require 'informant-common'

module InformantRails
  autoload :Config,             'informant-rails/config'
  autoload :Diagnostic,         'informant-rails/diagnostic'
  autoload :Middleware,         'informant-rails/middleware'
  autoload :RequestTracking,    'informant-rails/request_tracking'
  autoload :ValidationTracking, 'informant-rails/validation_tracking'
  autoload :VERSION,            'informant-rails/version'
end

require 'informant-rails/railtie'
