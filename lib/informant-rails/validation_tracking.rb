module InformantRails
  module ValidationTracking
    extend ActiveSupport::Concern

    included do
      include InformantCommon::ValidationTracking
    end
  end
end
