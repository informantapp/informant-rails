module InformantRails
  class Railtie < ::Rails::Railtie
    initializer 'informant' do |config|
      if Config.enabled?
        InformantCommon::Client.transmit(
          InformantCommon::Event::AgentInfo.new(
            agent_identifier: "informant-rails-#{InformantRails::VERSION}",
            framework_version: "rails-#{Rails.version}"
          )
        )
        InformantRails::Config.filter_parameters = Rails.configuration.filter_parameters

        config.middleware.use InformantRails::Middleware

        ActiveSupport.on_load(:action_controller) do
          include InformantRails::RequestTracking
        end

        ActiveSupport.on_load(:active_record) do
          include InformantRails::ValidationTracking
        end

        ActiveSupport.on_load(:mongoid) do
          include InformantRails::ValidationTracking
        end
      end
    rescue StandardError => e
      puts "Unable to bootstrap informant: #{e.message}"
    end
  end
end
