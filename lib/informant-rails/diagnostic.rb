class TestClass
  include ActiveModel::Model

  attr_accessor :field_name

  validates_presence_of :field_name
end

module InformantRails
  class Diagnostic
    def self.run
      new.run
    end

    def run
      if Config.api_token.blank?
        Rails.logger.info missing_api_token_message
      else
        response = InformantCommon::Client.transmit(test_form_submission).join.value
        display_response_message(response)
      end

      Rails.logger.info assistance_message
    end

    private

    def missing_api_token_message
      %<
        Your API token could not be found in the configuration.
        You can retrieve it from your Informantapp.com dashboard.

          Then add it to your server's environment as `INFORMANT_API_KEY`

          OR

          Set it manually with an initializer (config/informant.rb)
            InformantRails::Config.api_token = 'your token'
      >
    end

    def bad_response_message(server_message)
      "Seems like we have a problem. \"#{server_message}\" in this case."
    end

    def success_message
      'Everything looks good. You should see a test request on your dashboard.'
    end

    def assistance_message
      "If you need assistance or have any questions, send an email to support@informantapp.com and we'll help you out!"
    end

    def test_form_submission
      InformantCommon::Event::FormSubmission.new.tap do |form_submission|
        form_submission.handler = 'Connectivity#test'
        form_submission.process_model(InformantCommon::Model::ActiveModel.new(TestClass.new.tap(&:valid?)))
      end
    end

    def display_response_message(response)
      if response.code == '204'
        Rails.logger.info success_message
      else
        Rails.logger.info bad_response_message(response.body)
      end
    end
  end
end
