module InformantRails
  module RequestTracking
    extend ActiveSupport::Concern

    included do
      if defined? before_action
        before_action do
          InformantCommon::Client.current_transaction&.handler = [controller_name, action_name].join('#')
        end
      else
        before_filter do
          InformantCommon::Client.current_transaction&.handler = [controller_name, action_name].join('#')
        end
      end
    end
  end
end
