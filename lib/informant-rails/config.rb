module InformantRails
  module Config
    class << self
      delegate :api_token, :api_token=,
               :collector_host,
               :enabled?,
               :exclude_models, :exclude_models=,
               :filter_parameters, :filter_parameters=,
               :value_tracking?, :value_tracking=,
               to: InformantCommon::Config
    end
  end
end
