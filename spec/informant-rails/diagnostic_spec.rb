require 'spec_helper'

describe InformantRails::Diagnostic do
  let(:diagnostic) { described_class.new }

  let!(:request_stub) do
    stub_request(:post, 'https://collector-api.informantapp.com/v2/form-submissions')
      .with(
        body: {
          name: 'Connectivity#test',
          models: [{
            name: 'TestClass',
            errors: [{
              name: 'field_name',
              value: nil,
              message: "can't be blank"
            }]
          }]
        }
      )
  end

  before do
    allow(diagnostic).to receive(:assistance_message)
  end

  context 'with a missing api token' do
    it 'does not make a request' do
      InformantRails::Config.api_token = ''
      allow(diagnostic).to receive(:missing_api_token_message)
      diagnostic.run
      expect(diagnostic).to have_received(:missing_api_token_message)
      expect(request_stub).not_to have_been_requested
    end
  end

  context 'with an error on the server' do
    it 'displays the error' do
      request_stub.to_return(status: 401, body: 'Terrible Things')
      allow(diagnostic).to receive(:bad_response_message)
      diagnostic.run
      expect(diagnostic).to have_received(:bad_response_message).with('Terrible Things')
      expect(request_stub).to have_been_requested
    end
  end

  context 'with a successful response' do
    it 'displays a success message' do
      request_stub.to_return(status: 204)
      allow(diagnostic).to receive(:success_message)
      diagnostic.run
      expect(diagnostic).to have_received(:success_message)
      expect(request_stub).to have_been_requested
    end
  end
end
