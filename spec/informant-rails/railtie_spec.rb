require 'spec_helper'

describe InformantRails::Railtie do
  describe '#initializer' do
    let(:agent_info_json) do
      InformantCommon::Event::AgentInfo.new(
        agent_identifier: "informant-rails-#{InformantRails::VERSION}",
        framework_version: "rails-#{Rails.version}"
      ).to_json
    end

    let!(:agent_info_request_stub) do
      stub_request(:post, 'https://collector-api.informantapp.com/v2/client-info')
        .with(body: agent_info_json)
    end

    before do
      InformantRails::Config.api_token = 'abc123'
      InformantCommon::Client.enable!
      described_class.initializers.each(&:run)

      # This waits for the async agent stub
      sleep(0.5.seconds)
    end

    it 'sends the agent info' do
      expect(agent_info_request_stub).to have_been_requested
    end
  end
end
