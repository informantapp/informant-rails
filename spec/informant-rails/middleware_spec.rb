require 'spec_helper'

describe InformantRails::Middleware do
  describe '#call' do
    let(:middleware) { described_class.new(app) }
    let(:app) { double }
    let(:env) { { 'REQUEST_METHOD' => 'POST' } }
    let(:response) { double }

    before do
      allow(InformantCommon::Client).to receive(:start_transaction!).with('POST')
      allow(app).to receive(:call).with(env).and_return(response)
      allow(InformantCommon::Client).to receive(:process)
    end

    it 'wraps the request in an informant transaction' do
      expect(middleware.call(env)).to eq(response)
      expect(InformantCommon::Client).to have_received(:start_transaction!).with('POST')
      expect(app).to have_received(:call).with(env)
      expect(InformantCommon::Client).to have_received(:process)
    end
  end
end
