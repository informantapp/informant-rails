require 'bundler/setup'
require 'webmock/rspec'

Bundler.require

class TestSuiteCapabilities
  def self.active_record?
    @active_record ||= defined?(ActiveRecord)
  end

  def self.mongoid?
    @mongoid ||= defined?(Mongoid)
  end

  def self.enabled?(capability)
    return true if capability.blank?

    public_send("#{capability}?")
  end
end

ENV['INFORMANT_API_KEY'] = 'abc123'
require 'informant-rails'

Dir[File.expand_path(File.join(File.dirname(__FILE__), 'support', '**', '*.rb'))].sort.each { |f| require f }

RSpec.configure do |config|
  config.before do
    TestMigration.up if TestSuiteCapabilities.active_record?

    clear_mongodb if TestSuiteCapabilities.mongoid?

    InformantCommon::Config.reset!
    InformantCommon::Client.reset_transaction!
    InformantCommon::Client.enable!
  end

  config.around do |example|
    example.run if TestSuiteCapabilities.enabled?(example.metadata[:depends_on])
  end
end
