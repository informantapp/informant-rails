if TestSuiteCapabilities.mongoid?
  Mongoid.load!('spec/support/mongoid.yml', :test)

  def clear_mongodb
    Mongoid::Config.purge!
  end

  class Item
    include Mongoid::Document
    include InformantRails::ValidationTracking

    validates_length_of :name, minimum: 2

    validate :base_error

    field :name

    attr_accessor :add_base_error

    def base_error
      errors.add(:base, 'This is a base error') if add_base_error
    end
  end
end
