### 2.5.0

* Drop support for Ruby 2.6 since it has reached EOL

### 2.4.0

* Update for Rails 7 compatibility

### 2.3.0

* Require Ruby 2.6 or later.
* Fix activemodel deprecations warnings around error objects.

### 2.2.2

* Only support Ruby and Rails versions supported by their maintainers.
* Require Rails 5.2.0 or later.
* Require Ruby 2.5 or later.
* Use common logic extracted out to `informant-common`.

### 2.1.0 ###

* Disable the gem when the API server responds with 401 unauthorized (bad API key)

### 2.0.1 ###

* Update to use new v2 API
* Add support for mongoid 6.2
* Drop support for Rails 3.0, Rails 3.1, and Mongoid 3.0
* Fixed issue where diagnostic wouldn't print to console

### 1.1.0 ###

* Support for Mongoid 5.1
* Update dev and test gem dependencies
* Update URL for reporting data to Informant

### 1.0.0 ###

* Long overdue major version bump
* Improved compatibility with older versions of rails
* Tracking your rails version to improve our support
* Change license to MIT

### 0.9.2 ###

* Prevent any initialization without an API key present

### 0.9.1 ###

* Change before_filter to before_action (Issue #3)

### 0.9.0 ###

* Support for Rails 5.0

### 0.8.0 ###

* Support for Rails 4.2

### 0.7.1 ###

* Make client identifier more easily accessible

### 0.7.0 ###

* Add a configuration option to completely disable field value tracking
* Replace typhoeus with Net::HTTP

### 0.6.0 ###

* Performance improvements
* Fixed a bug where a manually set API token may not be properly registered

### 0.5.0 ###

* Prevent initialization of hooks if API token is not present
* Add support for Mongoid
* Allow any ActiveModel object to take advantage of validation tracking

### 0.4.1 ###

* Require rake in informant-rails.rb

### 0.4.0 ###

* Add client identifier to payload
* Add rails 3.0 or higher requirement to gemspec
* Add rake task to verify connectivity
* Update API endpoint
