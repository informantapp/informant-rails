lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'informant-rails/version'

Gem::Specification.new do |s|
  s.name = 'informant-rails'
  s.version = InformantRails::VERSION
  s.license = 'MIT'

  s.authors = ['Informant, LLC']
  s.email = ['support@informantapp.com']
  s.description = 'The Informant tracks what users do wrong in your forms so you can make them better.'
  s.homepage = 'https://www.informantapp.com'
  s.require_paths = ['lib']
  s.summary = 'The Informant tracks server-side validation errors and gives you metrics you never dreamed of.'

  s.required_ruby_version = '>= 2.7.0'
  s.add_runtime_dependency('informant-common', '~> 1.2.0')
  s.add_runtime_dependency('rails', '>= 6.0.0', '< 8.0.0')

  s.add_development_dependency('appraisal')

  s.files = Dir.glob('lib/**/*') + %w[LICENSE README.markdown Rakefile]
  s.require_path = 'lib'
  s.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
